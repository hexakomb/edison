from django.shortcuts import render,redirect
from . import forms
from Leads.models import Lead
from Contacts.models import Contact

# Create your views here.
def addContact(request,lead_id):
    lead = Lead.objects.get(pk=lead_id)
    if request.method == 'POST':
        form = forms.AddContact(request.POST)
        
        if form.is_valid():
            contact = form.save(commit=False)
            contact.lead = lead
            contact.save()
            return redirect('leads:leadDetail', lead_id)
    else:
         form = forms.AddContact()
    return render(request, 'contacts/addContact.html', {'form':form, 'lead':lead})


def contacts(request,lead_id):
    lead = Lead.objects.get(pk=lead_id)
    contacts = Contact.objects.all().filter(lead__id=lead.id)
    return render(request,'contacts/contacts.html',{'contacts':contacts,'lead':lead})
