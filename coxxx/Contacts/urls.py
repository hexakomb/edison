from django.urls import path
from . import views

app_name='contacts'
urlpatterns = [
     path('<str:lead_id>/addcontact', views.addContact, name='addContact'),
     path('<str:lead_id>/contacts', views.contacts, name='contacts'),
]