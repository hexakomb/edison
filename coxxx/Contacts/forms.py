from django import forms
from . import models

class AddContact(forms.ModelForm):
    class Meta:
        model = models.Contact
        fields = ['phone', 'email','isPrimary']