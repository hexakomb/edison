from django.db import models
from phone_field import PhoneField
from Leads.models import Lead

# Create your models here.
class Contact(models.Model):
    phone = PhoneField(blank=True)
    email = models.EmailField()
    isPrimary = models.BooleanField(default=None)
    lead = models.ForeignKey(Lead, default=None , on_delete=models.CASCADE)


    def __str__(self):
        return self.lead.name + ' P contacts'
