from .utils import render_to_pdf
from django.views.generic import View
from django.http import HttpResponse
from django.shortcuts import render, redirect, HttpResponse
from .models import Lead
from Contacts.models import Contact
from Meetings.models import Meeting
from . import forms
from .forms import CreateLead
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .filters import leadFilter
from rest_framework.response import Response
from rest_framework.decorators import api_view
from .serializers import LeadSerializer
from background_task import background
from django.conf import settings
import schedule
import time
import json
import urllib
from django.core import serializers
from django.core.serializers import serialize
from django.http import JsonResponse
from django.forms.models import model_to_dict
from django.core.paginator import Paginator
from django.views.generic import View, ListView, UpdateView, CreateView, DeleteView, DetailView
import datetime

leads_url = 'leads:leadList'
create_lead_template = 'leads/createLead.html'


def generate_pdf(request, *args, **kwargs):
    data = {
        'today': datetime.date.today(),
        'amount': 39.99,
        'customer_name': 'Tusifu Edison',
        'order_id': 1233434,
    }
    pdf = render_to_pdf('leads/report.html', data)
    return HttpResponse(pdf, content_type='application/pdf')

# Create your views here.
# @login_required(login_url="/accounts/login/")
# def leadList(request):
#     leads = Lead.objects.all()
#     myFilter = leadFilter(request.GET, queryset=leads)
#     leads = myFilter.qs
#     return render(request, 'leads/leadList.html', {'leads':leads})


class leadList(ListView):
    model = Lead
    template_name = 'leads/leadList.html'
    paginate_by = 3
    context_object_name = 'leads'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["filter"] = leadFilter(
            self.request.GET, queryset=Lead.objects.all())
        return context

    def get_queryset(self):
        return leadFilter(self.request.GET, queryset=Lead.objects.all()).qs


# Class for creating the new Lead

class CreateLeadClass(CreateView):
    model = Lead
    template_name = create_lead_template
    form_class = CreateLead

    def form_valid(self, form):
        recaptcha_response = self.request.POST.get('g-recaptcha-response')
        url = 'https://www.google.com/recaptcha/api/siteverify'
        values = {
            'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
            'response': recaptcha_response
        }
        data = urllib.parse.urlencode(values).encode()
        req = urllib.request.Request(url, data=data)
        response = urllib.request.urlopen(req)
        result = json.loads(response.read().decode())

        if result['success']:
            instance = form.save(commit=False)
            instance.creator = self.request.user
            instance.save()
            messages.success(self.request, 'lead successfully added')

            return redirect(leads_url)
        else:
            messages.error(self.request, 'invalid reCaptcha please try again')
            return redirect('leads:createLead')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        context['head'] = 'Create'
        return context


class UpdateLeadClass(UpdateView):
    model = Lead
    template_name = create_lead_template
    form_class = CreateLead

    def form_valid(self, form):
        recaptcha_response = self.request.POST.get('g-recaptcha-response')
        url = 'https://www.google.com/recaptcha/api/siteverify'
        values = {
            'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
            'response': recaptcha_response
        }
        data = urllib.parse.urlencode(values).encode()
        req = urllib.request.Request(url, data=data)
        response = urllib.request.urlopen(req)
        result = json.loads(response.read().decode())

        if result['success']:
            instance = form.save(commit=False)
            instance.creator = self.request.user
            instance.save()
            messages.success(self.request, 'lead successfully added')

            return redirect(leads_url)
        else:
            messages.error(self.request, 'invalid reCaptcha please try again')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(**kwargs)
        context['head'] = 'update'
        return context


# @login_required(login_url="/accounts/login/")
# def createLead (request):
#     head='add '
#     if request.method == 'POST':
#         form = forms.CreateLead(request.POST)
#         if form.is_valid():

#             recaptcha_response = request.POST.get('g-recaptcha-response')
#             url = 'https://www.google.com/recaptcha/api/siteverify'
#             values = {
#                 'secret' : settings.GOOGLE_RECAPTCHA_SECRET_KEY,
#                 'response' : recaptcha_response
#             }
#             data = urllib.parse.urlencode(values).encode()
#             req = urllib.request.Request(url, data = data)
#             response = urllib.request.urlopen(req)
#             result = json.loads(response.read().decode())


#             if result['success']:
#                 instance = form.save(commit=False)
#                 instance.creator = request.user
#                 instance.save()
#                 messages.success(request, 'lead successfully added')

#                 return redirect(leads_url)
#             else:
#                 messages.error(request, 'invalid reCaptcha please try again')

#         else:
#             messages.warning(request, 'error occured')
#             return redirect('leads:createLead')
#     else:
#          form = forms.CreateLead()
#     return render(request, create_lead_template, {'form':form,'head':head})

@login_required(login_url="/accounts/login/")
def leadDetail(request, lead_id):
    lead = Lead.objects.get(pk=lead_id)
    contacts = Contact.objects.all().filter(lead__id=lead.id)
    meetings = Meeting.objects.all().filter(lead__id=lead.id)
    return render(request, 'leads/leadDetail.html', {'lead': lead, 'contacts': contacts, 'meetings': meetings})


@login_required(login_url="/accounts/login/")
def deleteLead(request, lead_id):
    lead = Lead.objects.get(pk=lead_id)
    lead.delete()
    return redirect(leads_url)


@login_required(login_url="/accounts/login/")
def updateLead(request, lead_id):
    head = 'update'
    lead = Lead.objects.get(pk=lead_id)
    form = forms.CreateLead(request.POST or None, instance=lead)
    if form.is_valid():
        form.save()
        return redirect(leads_url)
    return render(request, create_lead_template, {'form': form, 'head': head})


@api_view(['GET'])
def leads_api(request):
    leads = Lead.objects.all()
    serializer = LeadSerializer(leads, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def lead_api(request, lead_id):
    leads = Lead.objects.get(pk=lead_id)
    serializer = LeadSerializer(leads, many=False)
    return Response(serializer.data)


@api_view(['POST'])
def leadCreate_api(request):
    form = LeadSerializer(data=request.data)
    if form.is_valid():
        form.save()
    return Response(form.data)


@api_view(['POST'])
def leadUpdate_api(request, lead_id):
    lead = Lead.objects.get(pk=lead_id)
    form = LeadSerializer(instance=lead, data=request.data)
    if form.is_valid():
        form.save()
    return Response(form.data)


@background(schedule=60)
@api_view(['DELETE'])
def leadDelete_api(request, lead_id):
    lead = Lead.objects.get(pk=lead_id).toJSON()
    lead.delete()
    return Response('the lead deleted successfully')
