from django.test import SimpleTestCase
from django.urls import resolve, reverse
from Leads.views import leadList, CreateLeadClass, leadDetail, deleteLead,updateLead

class TestUrls(SimpleTestCase):

    def test_leadList(self):
        url = reverse ('leads:leadList')
        self.assertEquals(resolve(url), leadList)

    def test_createLead(self):
        url = reverse ('leads:createLead')
        self.assertEquals(resolve(url), CreateLeadClass)


    def test_leadDetail(self):
        url = reverse ('leads:leadDetail', args=([1]))
        self.assertEquals(resolve(url).func, leadDetail)

    def test_deleteLead(self):
        url = reverse ('leads:delete', args=([1]))
        self.assertEquals(resolve(url).func, deleteLead)

    def test_updateLead(self):
        url = reverse ('leads:update', args=([1]))
        self.assertEquals(resolve(url).func, updateLead)

