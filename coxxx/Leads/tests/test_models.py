from django.test import TestCase
from Leads.models import Lead
from django.contrib.auth.models import User

class TestModels(TestCase):
    def setUp(self):
        self.lead = Lead.objects.create(
            name = 'Real Madrid',
            description = 'spanish team',
            creator = User.objects.create(
                username = 'tusifu',
                password = 'tusifu@123'
            ),
            stage = 'lead'
        )

    def test_lead_name(self):
        self.assertEquals(self.lead.name, 'Real Madrid')


    def test_lead_description(self):
        self.assertEquals(self.lead.description, 'spanish team')


    def test_lead_stage(self):
        self.assertEquals(self.lead.stage, 'lead')

    def test_lead_creator_username(self):
        self.assertEquals(self.lead.creator.username, 'tusifu')

    def test_lead_creator_password(self):
        self.assertEquals(self.lead.creator.password, 'tusifu@123')

