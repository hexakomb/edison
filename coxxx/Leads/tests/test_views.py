from django.test import TestCase, Client
from django.urls import reverse

class TestViews(TestCase):

    def test_leads_list_view(self):
        self.assertTemplateUsed('leads/leadList.html')

    def test_lead_detail_view(self): 
        self.assertTemplateUsed('leads/leadDetail.html')

