from django import forms
from . import models


def is_anagram(x, y):
    return sorted(x) == sorted(y)


class CreateLead(forms.ModelForm):
    name = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control', 'placeholder': 'add the lead name'})
    )

    class Meta:
        model = models.Lead
        fields = ['name', 'stage', 'description']

        widgets = {
            #  'name':forms.TextInput(attrs={'class': 'form-control', 'placeholder':'add the lead name'}),
            'stage': forms.Select(attrs={'class': 'form-control'}),
            'description': forms.Textarea(attrs={'rows': 3, 'class': 'form-control', 'placeholder': 'please add the description'}),

        }

    def clean_name(self):
        data = self.cleaned_data.get('name')
        if len(data) < 3:
            raise forms.ValidationError(
                'lead name has to be greater than 3 characters ')
        return data
