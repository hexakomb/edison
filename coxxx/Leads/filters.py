import django_filters
from django_filters import CharFilter
from .models import Lead
from django.db.models import Q
from django.forms import ModelForm, Textarea, TextInput


class leadFilter(django_filters.FilterSet):
    q = django_filters.CharFilter(method='my_custom_filter', label='')
    class Meta:
        model = Lead 
        fields = ['q']

    def my_custom_filter(self, queryset, name, value):
        return Lead.objects.filter(
            Q(name__icontains=value) | Q(description__icontains=value)| Q(stage__icontains=value) 
        )