from django.urls import path
from Leads import views

app_name='leads'
urlpatterns = [
    path('', views.leadList.as_view(), name='leadList'),
    # path('', views.leadList, name='leadList'),
    # path('create/', views.createLead, name='createLead'),
    path('create/', views.CreateLeadClass.as_view(), name='createLead'),
    path('pdf/', views.generate_pdf, name='pdf'),
    # path('removeUser/', views.job_that_executes_once, name='remove'),
    path('<str:lead_id>', views.leadDetail, name='leadDetail'),
    path('update/<str:lead_id>', views.updateLead, name="update"),
    path('delete/<str:lead_id>', views.deleteLead, name="delete"),
]