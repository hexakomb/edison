from django.urls import path
from Leads import views


# urlpatterns = [
#     path('api/', views.leads_api, name='leads_api'),
#     path('api/create', views.leadCreate_api, name='leadCreate_api'),
#     # path('api/removepost', views.removepost, name='removepost'),
#     path('api/update/<str:lead_id>', views.leadUpdate_api, name='leadUpdate_api'),
#     path('api/delete/<str:lead_id>', views.leadDelete_api, name='leadDelete_api'),
#     path('api/lead/<str:lead_id>', views.lead_api, name='lead_api'),
# ]