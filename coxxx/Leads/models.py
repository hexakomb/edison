from django.db import models
from django.utils.translation import gettext_lazy as _
from django.urls import reverse
from django.contrib.auth.models import User

# Create your models here.
class Lead(models.Model):
    name = models.CharField(max_length=100, default="", blank=False, null= False)
    creator = models.ForeignKey(User, default=None, on_delete=models.CASCADE)
    description = models.TextField(default="")
    class stageChoice(models.TextChoices):
        Lead = 'lead', _('Lead')
        Potential = 'potential', _('Potential')
        Opportunity = 'opportunity', _('Opportunity')
        Customer = 'customer', _('Customer')


    stage = models.CharField(
        max_length=100,
        choices=stageChoice.choices,
        default=stageChoice.Lead,
    )

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('leadDetail', args=[str(self.id)])


         