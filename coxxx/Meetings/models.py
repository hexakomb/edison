from django.db import models
from Leads.models import Lead
from django.urls import reverse

# Create your models here.
class Meeting(models.Model):
    title = models.CharField(max_length=100, default="")
    venue = models.CharField(max_length=100, default="")
    about = models.CharField(max_length=100, default="")
    conclusion = models.TextField(default="")
    lead = models.ForeignKey(Lead, on_delete=models.CASCADE, default=None )
    meetingMinute = models.FileField(default=None, blank=True)
    time = models.DateTimeField(default=None)
   

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('meeting_detail', args=[str(self.id)])

    class Meta:
        permissions = (('change time','Can change the time of meeting'),
        ('specify number of attendees','Can generate the number of users who attend the meeting'))