import django_filters 
from .models import Meeting
from django_filters import DateRangeFilter, DateFilter


class MeetingFilter(django_filters.FilterSet):
    start_date = DateFilter(field_name='time', lookup_expr=('gte'), label='start date') 
    end_date = DateFilter(field_name='time', lookup_expr=('lte'), label='end date')
    date_range = DateRangeFilter(field_name='time')

    class Meta:
        model = Meeting
        fields =[]