from django.shortcuts import render,redirect
from . import forms
from django.contrib.auth.decorators import login_required
from .models import Meeting
from Leads.models import Lead
from .filters import MeetingFilter
from formtools.wizard.views import SessionWizardView
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import Permission

class MeetingWizard(SessionWizardView):
    template_name="addMeeting.html"


    def done(self,form_list,**kwargs):
            return render(self.request,'meetings.html')

# Create your views here.
@login_required(login_url="/accounts/login/")
@permission_required('Meetings.change time')
def addMeeting (request,lead_id):
    lead = Lead.objects.get(pk=lead_id)
    if request.method == 'POST':
        form = forms.AddMeeting(request.POST, request.FILES)
        
        if form.is_valid():
            meeting = form.save(commit=False)
            meeting.lead = lead
            meeting.save()
            return redirect('leads:leadDetail', lead_id)
    else:
         form = forms.AddMeeting()
    return render(request, 'meetings/addMeeting.html', {'form':form, 'lead':lead})

def meetingList(request):
    meetings = Meeting.objects.all()
    myFilter = MeetingFilter(request.GET, queryset=meetings)
    meetings = myFilter.qs
    return render(request, 'meetings/meetings.html', {'meetings':meetings,'myFilter':myFilter})

@permission_required('Meetings.change time')
def meetings(request,lead_id):
    lead = Lead.objects.get(pk=lead_id)
    meetings = Meeting.objects.all().filter(lead__id=lead.id)
    return render(request,'meetings/meetings.html', {'meetings':meetings, 'lead':lead})


def all_permissions(request):
    permissions = Permission.objects.all()
    return render(request, 'meetings/permissions.html', {'permissions':permissions})
