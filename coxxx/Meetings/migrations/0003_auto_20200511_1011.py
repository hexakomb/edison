# Generated by Django 3.0.5 on 2020-05-11 10:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Meetings', '0002_meeting_conclusion'),
    ]

    operations = [
        migrations.AlterField(
            model_name='meeting',
            name='meetingMinute',
            field=models.FileField(blank=True, default=None, upload_to=''),
        ),
    ]
