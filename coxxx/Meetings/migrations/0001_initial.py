# Generated by Django 3.0.5 on 2020-05-07 21:34

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('Leads', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Meeting',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(default='', max_length=100)),
                ('venue', models.CharField(default='', max_length=100)),
                ('about', models.CharField(default='', max_length=100)),
                ('meetingMinute', models.ImageField(blank=True, default='walk.png', upload_to='')),
                ('time', models.DateTimeField(auto_now_add=True)),
                ('lead', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='Leads.Lead')),
            ],
        ),
    ]
