from django.urls import path
from . import views

app_name='meetings'
urlpatterns = [
    
    path('', views.meetingList, name='meetingList'),
    path('permissions', views.all_permissions, name='permissions'),
    path('<str:lead_id>/addmeeting', views.addMeeting, name='addMeeting'),
    path('<str:lead_id>/meetings', views.meetings, name='meetings'),

]