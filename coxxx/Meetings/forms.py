from django import forms
from . import models


class DateInput(forms.DateTimeField):
    input_type = 'date'


class AddMeeting(forms.ModelForm):
    class Meta:
        model = models.Meeting
        fields = ['title', 'venue', 'about',
                  'conclusion', 'time', 'meetingMinute']

        widgets = {
            'conclusion': forms.Textarea(attrs={'rows': 3, 'placeholder': 'add conclusion'}),
            'time': forms.DateInput(format='%Y-%m-%d%H:%M')
        }
